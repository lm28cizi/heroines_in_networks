Deskriptive Analyse der Daten
Fragestellung verfeinern, Netzwerkmethodik ausformulieren und versenden

# Notes

Verortung sowohl im sozialen Geflecht aber vielleicht auch vom Universum abhängig? (Vgl. [1]C. Stegbauer, Ed., Netzwerkanalyse und Netzwerktheorie: ein neues Paradigma in den Sozialwissenschaften, 2. Auflage. Wiesbaden: VS Verlag für Sozialwissenschaften, 2010. S.240 f.)


Wie lösen wir das Problem, dass wir nur Beziehungen zwischen den Figuren und Ausgaben haben, aber nicht zwischen den Figuren selbst?

Arbeiten wir mit gewichteten Beziehungen? In Abhängigkeit der Rolle in der Ausgabe?

Was wollen wir messen? Welche Werte und was sollen die dann aussagen? Zentralitätswert, betweenness

Machen wir nur eine Gegenüberstellung dieser Werte und schauen, wie sehr hoch und niedrig mit dem Geschlecht korrelliert?


# Generelle Untersuchung des Netzwerks:
* Kompaktheit
* Anzahl der Gruppen
* Transitivität

# Untersuchung der Knoten
* Zentratität (da ungerichteter Graph kein Prestige)
  * Grad, Closeness, Betweenness?


# Methodik

* Wie ist der Zusammenhang zwischen den Zentralitätswerten und dem Geschlecht der Charaktere?
* Betrachtung des gesamten Netzwerks, als auch der Netzwerke für die Ausgaben in Dekaden gruppiert
* Errechnen der Zentralitätswerte Degree, Betweenness & Closeness zur Bestimmung der Relevanz der Charaktere und deren Bedeutung für das Netzwerk
* Betrachtung sowohl für uni- als auch bimodales Netzwerk
* Verbindungen sind ungewichtet
* Vergleich der Werte zwischen den einzelnen Dekaden - lässt sich eine Veränderung in den Werten messen?
  * Sind die Unterschiede signifikant?
* Verwendung von Python & Networkx, das bietet Werkzeuge um mit bimodalen Grafen zu arbeiten (errechnet hier normalisierte Zentralitätswerte)


# Fragestellung

* Lässt sich mithilfe von Zentralitätswerten eine Veränderung in der netzwerktechnischen Stellung von weiblichen Charakteren in den Comicuniversen von Marvel und DC feststellen?
* Lässt sich eine Veränderung der Position weiblicher Charaktere in den Netzwerken der Comicuniversen von Marvel und DC im zeitlichen Verlauf feststellen?
* Lässt sich eine Verbesserung der Zentralitätswerte weiblicher Charaktere in den Netzwerken der Comicuniversen von Marvel und DC im zeitlichen Verlauf feststellen?
