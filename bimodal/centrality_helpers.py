# imports
import pandas as pd
import networkx as nx
from networkx.algorithms import bipartite
from bimodal.helpers import create_logger, GENDER, PART


def analysis(B, characters_df, path):
    # Get largest connected component and do there the analysis for now
    if PART == 'largest_cc':
        G = B.subgraph(max(nx.connected_components(B), key=len))
        get_gender_stat(B, G, path)
        get_components_stat(B, G, path)
    else:
        G = B

    character_nodes, comic_nodes = get_character_comic_nodes(G)

    degree, betweenness, closeness = get_centrality(G, character_nodes)
    nx.set_node_attributes(G, degree, 'degree')
    nx.set_node_attributes(G, betweenness, 'betweenness')
    nx.set_node_attributes(G, closeness, 'closeness')

    degree = pd.Series(degree)
    characters_df['degree'] = characters_df.index.map(degree)
    betweenness = pd.Series(betweenness)
    characters_df['betweenness'] = characters_df.index.map(betweenness)
    closeness = pd.Series(closeness)
    characters_df['closeness'] = characters_df.index.map(closeness)

    characters_df.to_csv(path + '.csv')


def create_graph(characters_arr, characters_attr, comics_arr, comics_attr, edges_arr):
    G = nx.Graph()
    G.add_nodes_from(characters_arr, bipartite=0)
    nx.set_node_attributes(G, characters_attr)
    G.add_nodes_from(comics_arr, bipartite=1)
    nx.set_node_attributes(G, comics_attr)
    G.add_edges_from(edges_arr)

    # Remove unclear entries (they came back in through the list of edges)

    lost_nodes = {n for n, d in G.nodes(data=True) if d.get('bipartite') is None}
    G.remove_nodes_from(lost_nodes)

    return G


def get_attr(characters_df, comics_df):
    characters_attr = characters_df.to_dict('index')
    comics_attr = comics_df.to_dict('index')

    return characters_attr, comics_attr


def get_centrality(_B, character_nodes):
    degree = bipartite.degree_centrality(_B, character_nodes)
    print(isinstance(degree, dict))
    betweenness = bipartite.betweenness_centrality(_B, character_nodes)
    print(isinstance(betweenness, dict))
    closeness = bipartite.closeness_centrality(_B, character_nodes)
    print(isinstance(closeness, dict))

    return degree, betweenness, closeness


def get_character_comic_nodes(_B):
    character_nodes = {n for n, d in _B.nodes(data=True) if d.get('bipartite') == 0}
    comic_nodes = set(_B) - character_nodes

    return character_nodes, comic_nodes


def get_components_stat(_B, _largest_cc, path):
    B_character_nodes, B_comic_nodes = get_character_comic_nodes(_B)
    largest_cc_character_nodes, largest_cc_comic_nodes = get_character_comic_nodes(_largest_cc)
    components_stat_df = pd.DataFrame([[len(B_character_nodes), len(largest_cc_character_nodes)],
                                       [len(B_comic_nodes), len(largest_cc_comic_nodes)]],
                                      index=['characters', 'issues'], columns=['whole', 'largest_cc'])

    components_stat_df.to_csv('../data/' + path + '_component_stat.csv')


def get_dfs(not_drop_duplicates=False):
    characters_df = pd.read_csv('../comics/nodes_characters_patched.csv')
    comics_df = pd.read_csv('../comics/nodes_stories.csv')
    edges_df = pd.read_csv('../comics/edges.csv')

    if not not_drop_duplicates:
        comics_df.drop_duplicates(subset=['id'], keep=False, inplace=True, ignore_index=True)

    return characters_df, comics_df, edges_df


def get_edges(_edges_df, weighted=False):
    if not weighted:
        return _edges_df[['from', 'to']].to_records(index=False)

    _edges_df['weight'] = _edges_df['position'].apply(lambda w: 2 if w == 'Featured' or w == 'Antagonist' else 1)
    _edges_attr = _edges_df.loc[:, ['weight']].to_dict('records')
    return list(zip(_edges_df['from'].to_numpy(), _edges_df['to'].to_numpy(), _edges_attr))


def get_gendered_nodes(_B):
    females = {n for n, d in _B.nodes(data=True) if d.get('gender') == 'female'}
    males = {n for n, d in _B.nodes(data=True) if d.get('gender') == 'male'}
    na = {n for n, d in _B.nodes(data=True) if d.get('gender') is None}
    divers = set(_B) - na - females - males

    return females, males, divers, na


def get_gender_stat(_B, _largest_cc, path):
    B_f, B_m, B_d, B_na = get_gendered_nodes(_B)
    largest_cc_f, largest_cc_m, largest_cc_d, largest_cc_na = get_gendered_nodes(_largest_cc)
    gender_stat_df = pd.DataFrame(
        [[len(B_f), len(largest_cc_f)], [len(B_m), len(largest_cc_m)], [len(B_d), len(largest_cc_d)],
         [len(B_na), len(largest_cc_na)]],
        index=GENDER, columns=['whole', 'largest_cc'])

    gender_stat_df.to_csv('../data/' + path + '_gender_stat.csv')


def get_subgraphs(_B):
    logger = create_logger(_B.graph.get('name'), '../data/' + _B.graph.get('name') + '.log')
    sg_list = [_B.subgraph(c) for c in sorted(nx.connected_components(_B), key=len, reverse=True)]

    for sg in sg_list:
        females, males, divers, na = get_gendered_nodes(sg)

        logger.info('nodes: {}'.format(len(sg.nodes())))
        logger.info('female: {}, male: {}, divers: {}, na: {}'.format(len(females), len(males), len(divers), len(na)))
