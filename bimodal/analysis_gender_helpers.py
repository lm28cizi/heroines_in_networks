# imports
import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import ks_2samp, levene, mannwhitneyu

from bimodal.helpers import DECADES, CENTRALITY, PART, IFE


def get_df(u_list):
    mux = u_list[0][1].index
    mux.names = ['gender', 'centrality']
    df = pd.DataFrame(index=mux)
    for d, series in u_list:
        df[d] = pd.DataFrame(series)
    df.dropna(inplace=True)

    return df


def get_gendered_df(u, d):
    df = pd.read_csv('../data/by_decade_' + PART + '/' + d + '_characters_' + u + '.csv', index_col=0,
                     usecols=['id', 'gender', 'degree', 'betweenness', 'closeness'])
    df_f = [g_df for g, g_df in df.groupby('gender') if g == 'female'].pop()
    df_m = [g_df for g, g_df in df.groupby('gender') if g == 'male'].pop()

    return df_m, df_f


def get_means(u, d):
    df = pd.read_csv('../data/by_decade_' + PART + '/' + d + '_characters_' + u + '.csv', index_col=0,
                     usecols=['id', 'gender', 'degree', 'betweenness', 'closeness'])
    df.loc[df['gender'] == 'genderfluid', ['gender']] = 'other'
    mean = {}
    for g, g_df in df.groupby('gender'):
        mean[g] = g_df[['degree', 'betweenness', 'closeness']].mean()

    return u, d, pd.DataFrame.from_dict(mean).T.stack()


def plot_dis(u, d):
    df_m, df_f = get_gendered_df(u, d)
    histograms, a_histo = plt.subplots(3, 2)
    boxplots, a_box = plt.subplots(3, 1)
    for i, c in enumerate(CENTRALITY):
        a_histo[i, 0].hist(df_m[c], bins='stone', density=True)
        a_histo[i, 1].hist(df_f[c], bins='stone', density=True, color='tab:orange')
        a_histo[i, 0].set_ylabel(c)
        a_box[i].boxplot([df_m[c], df_f[c]], sym='', vert=False)
        a_box[i].set_ylabel(c)
        a_box[i].set_yticklabels(['male', 'female'])

    histograms.savefig('plots/dist_' + PART + '/histograms_' + u + '_' + d + '.' + IFE, bbox_inches='tight')
    boxplots.savefig('plots/dist_' + PART + '/boxplots_' + u + '_' + d + '.' + IFE, bbox_inches='tight')


def plot_gender_stat(u):
    def plot_func(x):
        ax_sub = graph[x]
        plot = df[x].unstack().plot(kind='bar', stacked='True', ax=ax_sub, legend=False)
        ax_sub.set_xlabel(x, weight='bold')
        return plot

    df_largest_cc = pd.DataFrame
    df_whole = pd.DataFrame
    for i, d in enumerate(DECADES):
        df = pd.read_csv('../data/by_decade_largest_cc/' + d + '_characters_' + u + '_gender_stat.csv', index_col=0)
        if i == 0:
            df_largest_cc = pd.DataFrame(df['largest_cc']).copy()
            df_largest_cc.rename(columns={'largest_cc': d}, inplace=True)
            df_whole = pd.DataFrame(df['whole']).copy()
            df_whole.rename(columns={'whole': d}, inplace=True)
        else:
            df_largest_cc[d] = df['largest_cc']
            df_whole[d] = df['whole']

    for d in DECADES:
        df_largest_cc[d] = (df_largest_cc[d] / df_largest_cc[d].sum()) * 100
        df_whole[d] = (df_whole[d] / df_whole[d].sum()) * 100

    d = {'largest_cc': df_largest_cc, 'whole': df_whole}
    df = pd.concat(d.values(), keys=d.keys())
    df.index.names = ['component', 'gender']

    n_subplots = len(df.columns)
    fig, ax = plt.subplots(nrows=1, ncols=n_subplots, sharey=True, figsize=(16, 9))
    graph = dict(zip(df.columns, ax))
    list(map(lambda x: plot_func(x), graph))
    fig.subplots_adjust(wspace=0)
    plt.legend()
    fig.savefig('plots/gender_ratio_' + u + '.' + IFE, bbox_inches='tight')


def plot_means(u, u_list):
    df = get_df(u_list)
    plot, a = plt.subplots(1, 3, figsize=(16, 9))

    for i, c in enumerate(CENTRALITY):
        df.xs(c, level='centrality').T.plot(ax=a[i], legend=False).get_figure()
        a[i].set_title(c)
    plt.legend()
    plot.savefig('plots/dist_' + PART + '/gender_means_' + u + '.' + IFE, bbox_inches='tight')


def stat_ks_test(u, d):
    df_m, df_f = get_gendered_df(u, d)
    p_list = []
    for i, c in enumerate(CENTRALITY):
        stat, p_val = ks_2samp(df_m[c], df_f[c])
        p_list.append(p_val)

    return u, d, p_list


def stat_l_test(u, d):
    df_m, df_f = get_gendered_df(u, d)
    p_list = []
    for i, c in enumerate(CENTRALITY):
        stat, p_val = levene(df_m[c], df_f[c])
        p_list.append(p_val)

    return u, d, p_list


def stat_mwu_test(u, d):
    df_m, df_f = get_gendered_df(u, d)
    p_list = []
    for i, c in enumerate(CENTRALITY):
        stat, p_val = mannwhitneyu(df_m[c], df_f[c])
        p_list.append(p_val)

    return u, d, p_list
