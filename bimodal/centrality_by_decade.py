# imports
import pandas as pd
from multiprocessing import Pool
from bimodal.centrality_helpers import get_dfs, get_attr, get_edges, create_graph, analysis
from bimodal.helpers import PARAMS, PART


def prepare_analysis(u, d):
    _edges_df = pd.read_csv('../comics/by_decade/' + d + '_' + u + '_edges.csv')
    edges_arr = get_edges(_edges_df)

    characters_from_edges_df = characters_df[characters_df.index.isin(_edges_df['from'])]
    comics_from_edges_df = comics_df[comics_df.index.isin(_edges_df['to'])]
    characters_attr, comics_attr = get_attr(characters_from_edges_df, comics_from_edges_df)

    G = create_graph(characters_from_edges_df.index, characters_attr, comics_from_edges_df.index, comics_attr, edges_arr)
    G.graph['name'] = u
    analysis(G, characters_from_edges_df.copy(), '../data/by_decade_' + PART + '/' + d + '_characters_' + u)


if __name__ == '__main__':
    duplicates_df = pd.read_csv('../comics/duplicates.csv', usecols=['id'])
    characters_df, comics_df, edges_df = get_dfs()

    characters_df.set_index('id', inplace=True)
    comics_df.set_index('id', inplace=True)

    # Do magic
    with Pool() as p:
        p.starmap_async(prepare_analysis, PARAMS).get()
