# imports
from multiprocessing import Pool
from bimodal.centrality_helpers import get_edges, get_dfs, split_by_publisher, get_attr, create_graph, analysis
from bimodal.helpers import set_logging


if __name__ == '__main__':
    set_logging()

    characters_df, comics_df, edges_df = get_dfs()

    characters_df.set_index('id', inplace=True)
    comics_df.set_index('id', inplace=True)
    characters_attr, comics_attr = get_attr(characters_df, comics_df)
    edges_arr = get_edges(edges_df, True)

    G = create_graph(characters_df.index, characters_attr, comics_df.index, comics_attr, edges_arr)

    G_list = split_by_publisher(G)
    paths = []
    df_list = []
    for B in G_list:
        paths.append('../data/characters_' + B.graph.get('name'))
        df_list.append(characters_df)

    # Do magic
    with Pool() as p:
        p.starmap(analysis, zip(G_list, df_list, paths))
