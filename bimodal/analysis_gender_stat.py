# imports
import pandas as pd
from multiprocessing import Pool
from bimodal.analysis_gender_helpers import stat_ks_test, stat_l_test, plot_dis, get_means, plot_means, \
    plot_gender_stat, stat_mwu_test
from bimodal.helpers import UNIVERSE, CENTRALITY, PARAMS, PART


def worker(f):
    result = p.starmap_async(f, PARAMS).get()

    M = [(d, p_list) for u, d, p_list in result if u == 'Marvel']
    DC = [(d, p_list) for u, d, p_list in result if u == 'DC']

    for u, u_list in [('Marvel', M), ('DC', DC)]:
        df = pd.DataFrame.from_dict(dict(u_list))
        df['centrality'] = pd.Series(CENTRALITY)
        df.set_index('centrality', inplace=True)
        df.to_csv('../data/p_values_' + PART + '/' + f.__name__ + '_' + u + '.csv')


if __name__ == '__main__':
    with Pool() as p:
        p.map(plot_gender_stat, UNIVERSE)
        p.starmap_async(plot_dis, PARAMS).get()

        result = p.starmap_async(get_means, PARAMS).get()

        M = [(d, df) for u, d, df in result if u == 'Marvel']
        DC = [(d, df) for u, d, df in result if u == 'DC']

        p.starmap(plot_means, [('Marvel', M), ('DC', DC)])

        worker(stat_ks_test)
        worker(stat_l_test)
        worker(stat_mwu_test)
