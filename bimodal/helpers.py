# imports
import logging
from itertools import product


CENTRALITY = ['degree', 'betweenness', 'closeness']
DECADES = ['1960', '1970', '1980', '1990', '2000', '2010']
IFE = 'svg'  # image file extension ['pdf', 'png', 'svg']
GENDER = ['female', 'male', 'other', 'na']
UNIVERSE = ['Marvel', 'DC']
PARAMS = list(product(UNIVERSE, DECADES))
PART = 'whole'  # ['largest_cc', 'whole']


def create_logger(name, path):
    logger = logging.getLogger(name)
    fh = logging.FileHandler(path)
    fh.setLevel(logging.DEBUG)
    # fh.setFormatter(logging.Formatter('%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s'))
    logger.addHandler(fh)

    return logger


def set_logging():
    logging.basicConfig(filemode='w',
                        format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                        datefmt='%H:%M:%S',
                        level=logging.INFO)
