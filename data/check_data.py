import csv


def abs(publisher):
  gender = {}
  race = {}
  alignment = {}
  maritalstatus = {}

  for row in publisher:
    gender[row['gender']] = gender.get(row['gender'], 0) + 1
    race[row['race']] = race.get(row['race'], 0) + 1
    alignment[row['alignment']] = alignment.get(row['alignment'], 0) + 1
    maritalstatus[row['maritalstatus']] = maritalstatus.get(row['maritalstatus'], 0) + 1

  return {
    'gender': gender,
    'race': race,
    'alignment': alignment,
    'maritalstatus': maritalstatus
  }


def make_gender_statistic(publisher):
  gender_statistic = {}
  for row in publisher['rows']:
    gender_statistic[row['gender']] = gender_statistic.get(row['gender'], 0) + 1

  female = 0
  male = 0
  na = 0
  other = 0

  for gender in gender_statistic:
    if gender == 'female':
      female = gender_statistic[gender]
    elif gender == 'male':
      male = gender_statistic[gender]
    elif gender == 'NA':
      na += gender_statistic[gender]
    else:
      other += gender_statistic[gender]

  total = female + male + na + other
  print()
  print('{} female: {:2.2%}, male: {:2.2%}, na:{:2.2%}, other: {:2.2%}'.format(publisher['name'], female/total, male/total, na/total, other/total))
  print(list(gender_statistic))
  print('\n')

  return gender_statistic

def get_alignment(alignment_statistic, entry):
  alignment_statistic[entry['alignment'].lower()] = alignment_statistic.get(entry['alignment'].lower(), 0) + 1
  alignment_statistic['total'] = alignment_statistic.get('total', 0) + 1


def make_alignment_statistic(publisher):
  female = {}
  male = {}
  na = {}
  other = {}
    
  for row in publisher['rows']:
    if row['gender'] == 'female':
      get_alignment(female, row)
    elif row['gender'] == 'male':
      get_alignment(male, row)
    elif row['gender'] == 'NA':
      get_alignment(na, row)
    else:
      get_alignment(other, row)

  alignment_statistic = {
    'female': female,
    'male': male,
    'na': na,
    'other': other
  }

  print(publisher['name'])
  for gender in alignment_statistic:
    print(gender)
    g = alignment_statistic[gender]
    for key in g:
      if key != 'total':
        print('{}: {:2.2%}'.format(key, g[key]/g['total']))
    


def print_statistic(statistic):
  for name in statistic:
    publisher = statistic[name]
    print(name)
    for key in publisher:
      category = publisher[key]
      print(key)
      for entry in category:
        print(entry, category[entry])


dc = []
marvel = []


with open('../comics/nodes_characters.csv', 'r', encoding = 'utf-8') as population_csv:
  reader = csv.DictReader(population_csv)
  rows = list(reader)

  print(f'Total entries: {len(rows)}\n')

  for row in rows:
    if row['home_publisher'] == 'DC':
      dc.append(row)
    else:
      marvel.append(row)
    

DC = { 'name': 'DC', 'rows': dc }
MARVEL = { 'name': 'Marvel', 'rows': marvel }

make_gender_statistic(DC)
make_gender_statistic(MARVEL)

make_alignment_statistic(DC)
make_alignment_statistic(MARVEL)
