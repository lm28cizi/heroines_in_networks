## Merge our data with date from https://www.kaggle.com/fivethirtyeight/fivethirtyeight

library(readr)
library(dplyr)

nodes_characters <- read_csv("comics/nodes_characters.csv")
marvel_wikia_data <- read_csv("comics/marvel-wikia-data.csv")
dc_wikia_data <- read_csv("comics/dc-wikia-data.csv")

names(marvel_wikia_data) <- names(dc_wikia_data)
additional_data <- rbind(marvel_wikia_data, dc_wikia_data)

data_combined <- left_join(nodes_characters, additional_data, by = c("id" = "name"))

readr::write_csv2(data_combined, "comics/data_combined.csv")
