# imports
import numpy as np
from bimodal.centrality_helpers import get_dfs


if __name__ == '__main__':
    characters_df, comics_df, edges_df = get_dfs(True)
    duplicates = comics_df.drop_duplicates(subset=['id'], keep=False, ignore_index=True)
    duplicates = list(comics_df['id'])
    comics_df.drop_duplicates(subset=['id'], keep=False, inplace=True, ignore_index=True)
    comics_df.set_index('id', inplace=True)

    edges_wod_df = edges_df.loc[edges_df['to'].isin(duplicates)]

    edges_wd_df = edges_wod_df.join(comics_df[['year', 'publisher']], on='to')

    edges_wd_df['year'] = edges_wd_df['year'].apply(lambda y: y/10)
    edges_wd_df['year'] = edges_wd_df['year'].apply(np.floor)

    for decade, d_df in edges_wd_df.groupby('year'):
        for u, u_df in d_df.groupby('publisher'):
            u_df.drop(columns=['year', 'publisher']).to_csv('../comics/by_decade/' + str(int(decade)) + 'x_' + u + '_edges.csv')






