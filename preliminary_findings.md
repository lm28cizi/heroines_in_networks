# Some observations

## Statistical stuff

### Share of female characters by gender

DC has significantly more female characters compared to Marvel: 

```{r}
male_female_only <- nodes_characters %>%
    filter(gender == "male" | gender == "female")

table <- gmodels::CrossTable(male_female_only$gender, male_female_only$home_publisher)

chisq.test(table$t)

> 	Pearson's Chi-squared test with Yates' continuity correction
>
> data:  table$t
> X-squared = 49.68, df = 1, p-value = 1.81e-12

```

### Lifespan of characters

Calculating the lifespan of each character (in real world years) suggests there is a significant difference between male and female characters:

```{r}
character_lifespans <- nodes_characters %>%
    select(id, gender, home_publisher, first_year, last_year) %>%
    filter(!is.na(first_year) & !is.na(last_year) & !(last_year == 9999) & !(first_year == 9999)) %>%
    mutate(lifespan = last_year - first_year) %>%
    filter(lifespan >= 0 & (gender == "male" | gender == "female"))

wilcox.test(character_lifespans[character_lifespans$gender == "male",]$lifespan, character_lifespans[character_lifespans$gender == "female",]$lifespan)

>   Wilcoxon rank sum test with continuity correction
>
> data:  character_lifespans[character_lifespans$gender == "male", ]$lifespan and character_lifespans[character_lifespans$gender == "female", ] $lifespan
> W = 5881512, p-value = 0.001466
> alternative hypothesis: true location shift is not equal to 0
```

However I'm neither sure how appropriate Wilcox test is here (the data is heavily skewed) nor how good of a measurement for a character lifespan this is (I'm rather sure it is a pretty bad one).