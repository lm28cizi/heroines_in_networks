library(igraph)
library(dplyr)
library(doParallel)
library(ggplot2)

base_dir <- rprojroot::find_root("README.md")

# Load data
source("unimodal/import_data.R")

window <- 3 # Years to include on each side
years <- min(data$year, na.rm = TRUE):max(data$year, na.rm = TRUE)

# We start of with the DC Characters

data_dc <- data[data$home_publisher == "DC",]

dir.create(file.path(base_dir, "logs"))
cl <- makePSOCKcluster(detectCores(), outfile = file.path(base_dir, "logs", "rolling.log"))

# Export everything we need to the cluster

# clusterExport(cl = cl, list("index_file", "dates"))
clusterEvalQ(cl = cl, library(igraph))
clusterEvalQ(cl = cl, library(tictoc))
clusterEvalQ(cl = cl, library(dplyr))
clusterExport(cl = cl, varlist = c("data_dc", "nodes_characters", "window"))

dc_rolling <- parLapplyLB(cl, years, function(year) {
    cat(paste0("[PID ", Sys.getpid(), "] INFO: Processing year ", year, "...\n"))
    edges <- data_dc[data_dc$year %in% seq(year - window, year + window),] %>%
        relocate(to, .after = id)
    g <- graph.data.frame(edges, directed = FALSE)
    V(g)$type <- V(g)$name %in% edges$to
    g <- bipartite.projection(g, which = FALSE)
    V(g)$type <- nodes_characters[nodes_characters$id %in% V(g)$name,]$gender
    between.cent <- centr_betw(g)
    data.frame(name = V(g)$name,
               gender = V(g)$type,
               betweenness = between.cent$res,
               year = year,
               publisher = "DC")
})

dc_rolling <- do.call(rbind, dc_rolling)

tests_dc <- dc_rolling %>%
    group_by(year) %>%
    group_map(~ wilcox.test(.x[.x$gender == "female",]$betweenness, .x[.x$gender == "male",]$betweenness))

years_dc <- unique(dc_rolling$year)

table_dc <- lapply(1:length(years_dc), function(x) {
    res <- data.frame(year = years_dc[x],
                      n_female = nrow(dc_rolling[dc_rolling$gender == "female" & dc_rolling$year == years_dc[x],]),
                      mean_bet_female = mean(dc_rolling[dc_rolling$gender == "female" & dc_rolling$year == years_dc[x],]$betweenness),
                      n_male = nrow(dc_rolling[dc_rolling$gender == "male" & dc_rolling$year == years_dc[x],]),
                      mean_bet_male = mean(dc_rolling[dc_rolling$gender == "male" & dc_rolling$year == years_dc[x],]$betweenness),
                      W = tests_dc[[x]]$statistic,
                      p = tests_dc[[x]]$p.value
    )
})

table_dc <- do.call(rbind, table_dc)

# Now marvel

data_marvel <- data[data$home_publisher == "Marvel",]

clusterExport(cl, "data_marvel")

marvel_rolling <- parLapplyLB(cl, years, function(year) {
    cat(paste0("[PID ", Sys.getpid(), "] INFO: Processing year ", year, "...\n"))
    edges <- data_marvel[data_marvel$year %in% seq(year - window, year + window),] %>%
        relocate(to, .after = id)
    g <- graph.data.frame(edges, directed = FALSE)
    V(g)$type <- V(g)$name %in% edges$to
    g <- bipartite.projection(g, which = FALSE)
    V(g)$type <- nodes_characters[nodes_characters$id %in% V(g)$name,]$gender
    between.cent <- centr_betw(g)
    data.frame(name = V(g)$name,
               gender = V(g)$type,
               betweenness = between.cent$res,
               year = year,
               publisher = "Marvel")
})

marvel_rolling <- do.call(rbind, marvel_rolling)

tests_marvel <- marvel_rolling %>%
    group_by(year) %>%
    group_map(~ wilcox.test(.x[.x$gender == "female",]$betweenness, .x[.x$gender == "male",]$betweenness))

years_marvel <- unique(marvel_rolling$year)

table_marvel <- lapply(1:length(years_marvel), function(x) {
    res <- data.frame(year = years_marvel[x],
                      n_female = nrow(marvel_rolling[marvel_rolling$gender == "female" & marvel_rolling$year == years_marvel[x],]),
                      mean_bet_female = mean(marvel_rolling[marvel_rolling$gender == "female" & marvel_rolling$year == years_marvel[x],]$betweenness),
                      n_male = nrow(marvel_rolling[marvel_rolling$gender == "male" & marvel_rolling$year == years_marvel[x],]),
                      mean_bet_male = mean(marvel_rolling[marvel_rolling$gender == "male" & marvel_rolling$year == years_marvel[x],]$betweenness),
                      W = tests_marvel[[x]]$statistic,
                      p = tests_marvel[[x]]$p.value
    )
})

table_marvel <- do.call(rbind, table_marvel)

stopCluster(cl)

# Take a look at characters

dc_rolling$name <- gsub("\\(.*\\)$", "", dc_rolling$name) %>%
    trimws %>%
    tolower

dc_rolling <- dc_rolling %>%
    select(-publisher) %>%
    group_by(name, gender, year) %>%
    summarize(betweenness_sum = sum(betweenness), betweenness_mean = mean(betweenness)) %>%
    add_count(name) %>%
    group_by(name, gender, n) %>%
    summarize(centr = sum(betweenness_sum) / n) %>%
    unique


marvel_rolling$name <- gsub("\\(.*\\)$", "", marvel_rolling$name) %>%
    trimws %>%
    tolower

marvel_rolling <- marvel_rolling %>%
    select(-publisher) %>%
    group_by(name, gender, year) %>%
    summarize(betweenness_sum = sum(betweenness), betweenness_mean = mean(betweenness)) %>%
    add_count(name) %>%
    group_by(name, gender, n) %>%
    summarize(centr = sum(betweenness_sum) / n) %>%
    unique

rolling_character_betweenness <- function(data, window) {
    
    years <- min(data$year, na.rm = TRUE):max(data$year, na.rm = TRUE)
    
    dir.create(file.path(base_dir, "logs"))
    
    cl <- makePSOCKcluster(detectCores(), outfile = file.path(base_dir, "logs", "rolling.log"))
    
    clusterEvalQ(cl = cl, library(igraph))
    clusterEvalQ(cl = cl, library(tictoc))
    clusterEvalQ(cl = cl, library(dplyr))
    clusterExport(cl = cl, varlist = c("data", "gender_data", "window"))
    
    rolling <- parLapplyLB(cl, years, function(year) {
        cat(paste0("[PID ", Sys.getpid(), "] INFO: Processing year ", year, "...\n"))
        edges <- data[data$year %in% seq(year - window, year + window),] %>%
            relocate(to, .after = id)
        g <- graph.data.frame(edges, directed = FALSE)
        V(g)$type <- V(g)$name %in% edges$to
        g <- bipartite.projection(g, which = FALSE)
        V(g)$type <- gender_data[gender_data$id %in% V(g)$name,]$gender
        between.cent <- centr_betw(g)
        data.frame(name = V(g)$name,
                   gender = V(g)$type,
                   betweenness = between.cent$res,
                   year = year)
    })
    
    rolling <- do.call(rbind, rolling)
    
    rolling$name <- gsub("\\(.*\\)$", "", rolling$name) %>%
        trimws %>%
        tolower
    
    rolling <- rolling %>%
        group_by(name, gender, year) %>%
        summarize(betweenness_sum = sum(betweenness), betweenness_mean = mean(betweenness)) %>%
        add_count(name) %>%
        group_by(name, gender, n) %>%
        summarize(centr = sum(betweenness_sum) / n) %>%
        unique
    
    stopCluster(cl)
    
    rolling
}

dc_rolling <- data %>%
    filter(home_publisher == "DC") %>%
    rolling_character_betweenness(., window = 3)

table(dc_rolling$gender)

# Anteil: 0.4053699 (3699 / 9125)

dc_top_5p <- subset(dc_rolling, centr > quantile(centr, prob = 1 - 5/100))

table(dc_top_5p$gender)

# Anteil: 0.4266667

dc_1900 <-  data %>%
    filter(home_publisher == "DC") %>%
    filter(year < 2000) %>%
    rolling_character_betweenness(., window = 3)

table(dc_1900$gender)

# Anteil: 0.3594581

dc_1900_top5p <- subset(dc_1900, centr > quantile(centr, prob = 1 - 5/100))

table(dc_1900_top5p$gender)

# Anteil: 0.4178571

dc_2000 <- data %>%
    filter(home_publisher == "DC") %>%
    filter(year >= 2000) %>%
    rolling_character_betweenness(., window = 3)

table(dc_2000$gender)

# Anteil: 0.4471849

dc_2000_top5p <- subset(dc_2000, centr > quantile(centr, prob = 1 - 5/100))

table(dc_2000_top5p$gender)

# Anteil: 0.3954248

marvel_rolling <- data %>%
    filter(home_publisher == "Marvel") %>%
    rolling_character_betweenness(., window = 3)

table(marvel_rolling$gender)

# Anteil: 0.3429692

marvel_top_5p <- subset(marvel_rolling, centr > quantile(centr, prob = 1 - 5/100))

table(marvel_top_5p$gender)

# Anteil: 0.440585

marvel_1900 <-  data %>%
    filter(home_publisher == "Marvel") %>%
    filter(year < 2000) %>%
    rolling_character_betweenness(., window = 3)

table(marvel_1900$gender)

# Anteil: 0.3014265

marvel_1900_top5p <- subset(marvel_1900, centr > quantile(centr, prob = 1 - 5/100))

table(marvel_1900_top5p$gender)

# Anteil: 0.3813559

marvel_2000 <- data %>%
    filter(home_publisher == "Marvel") %>%
    filter(year >= 2000) %>%
    rolling_character_betweenness(., window = 3)

table(marvel_2000$gender)

# Anteil: 0.3886592

marvel_2000_top5p <- subset(marvel_2000, centr > quantile(centr, prob = 1 - 5/100))

table(marvel_2000_top5p$gender)

# Anteil: 0.440367