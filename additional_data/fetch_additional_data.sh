if ! command -v 7z &> /dev/null
then
    echo "7zip was not found but is required, exiting..."
    exit
fi

current_folder=$(basename $PWD)

if [ "$current_folder" = "heroines_in_networks" ]; then
    cd additional_data
fi

uri_marvel="https://s3.amazonaws.com/wikia_xml_dumps/e/en/enmarveldatabase_pages_current.xml.7z"
uri_dc="https://s3.amazonaws.com/wikia_xml_dumps/e/en/endcdatabase_pages_current.xml.7z"

file_marvel="enmarveldatabase_pages_current.xml.7z"
file_dc="endcdatabase_pages_current.xml.7z"

wget $uri_marvel -O $file_marvel
wget $uri_dc -O $file_dc

7z x $file_dc
7z x $file_marvel

rm $file_dc
rm $file_marvel