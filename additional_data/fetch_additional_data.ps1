## Script to download up-to-date data from the Marvel and DC Wikia Pages

if(!Get-Command 7za) {
    Write-Output "Please make sure that 7zip is available, exiting..."
    exit
}


$current_folder = Get-Location | Select-Object | %{$_.ProviderPath.Split("\")[-1]}

if($current_folder = "heroines_in_networks") {
    Set-Location "additional_data"
}

$uri_marvel = "https://s3.amazonaws.com/wikia_xml_dumps/e/en/enmarveldatabase_pages_current.xml.7z"
$uri_dc = "https://s3.amazonaws.com/wikia_xml_dumps/e/en/endcdatabase_pages_current.xml.7z"

$file_marvel = "enmarveldatabase_pages_current.xml.7z"
$file_dc = "endcdatabase_pages_current.xml.7z"

Start-BitsTransfer -Source $uri_marvel -Destination $file_marvel
Start-BitsTransfer -Source $uri_dc -Destination $file_dc

7za.exe e $file_marvel
7za.exe e $file_dc

Remove-Item $file_dc
Remove-Item $file_marvel
